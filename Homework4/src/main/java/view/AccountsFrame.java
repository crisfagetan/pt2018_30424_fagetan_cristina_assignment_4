package view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import model.Account;
import model.AccountSavings;
import model.AccountSpendings;
import model.Bank;
import model.Person;

public class AccountsFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Bank savingsManager;
	private Bank spendingsManager;
	private JTable savingsTable;
	private JTable spendingsTable;
	SavingsTableModel savingsTableModel;
	SpendingsTableModel spendingsTableModel;
	HashSet<Account> accounts = new HashSet<Account>();;
	HashSet<AccountSavings> savings = new HashSet<AccountSavings>();
	HashSet<AccountSpendings> spendings = new HashSet<AccountSpendings>();
	Person client = new Person();
	AccountSpendings oldspendings = new AccountSpendings();
	AccountSavings oldsavings = new AccountSavings();

	private JLabel cnpLabel = new JLabel("CNP:");
	private JLabel nameLabel = new JLabel("Client:");
	private JTextField clientCnp;
	private JTextField clientName;
	private JButton add1 = new JButton("ADD SAVINGS");
	private JButton delete1 = new JButton("DELETE SAVINGS");
	private JButton update1 = new JButton("UPDATE SAVINGS");
	private JButton add2 = new JButton("ADD SPENDINGS");
	private JButton delete2 = new JButton("DELETE SPENDINGS");
	private JButton update2 = new JButton("UPDATE SPENDINGS");

	private JButton depositSavings = new JButton("DEPOSIT SAVINGS");
	private JButton withdrawSavings = new JButton("WITHDRAW SAVINGS");
	private JButton depositSpendings = new JButton("DEPOSIT SPENDINGS");
	private JButton withdrawSpendings = new JButton("WITHDRAW SPENDINGS");

	private JLabel sumLabel = new JLabel("Sum:");
	private JTextField sum;

	private JTextField accountId;
	private JTextField accountName;
	private JTextField accountSum;
	private JTextField accountType;

	public AccountsFrame() {
		//
		savingsManager = new Bank();
		spendingsManager = new Bank();

		// --------TOP-----------------
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BorderLayout());

		// ------Customer Panel------
		JPanel customerPanel = new JPanel();
		FlowLayout topLayout = new FlowLayout();
		customerPanel.setLayout(topLayout);
		clientCnp = new JTextField();
		clientCnp.setColumns(20);
		clientCnp.setEditable(false);
		clientName = new JTextField();
		clientName.setColumns(20);
		clientName.setEditable(false);
		customerPanel.add(cnpLabel);
		customerPanel.add(clientCnp);
		customerPanel.add(nameLabel);
		customerPanel.add(clientName);
		customerPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		topPanel.add(customerPanel, BorderLayout.NORTH);

		// --------Savings Buttons-----------
		JPanel savingsButtonsPanel = new JPanel();
		savingsButtonsPanel.setLayout(topLayout);
		savingsButtonsPanel.add(add1);
		savingsButtonsPanel.add(update1);
		savingsButtonsPanel.add(delete1);
		savingsButtonsPanel.add(depositSavings);
		savingsButtonsPanel.add(withdrawSavings);
		savingsButtonsPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		topPanel.add(savingsButtonsPanel, BorderLayout.LINE_START);

		// --------Spendings Buttons-----------
		JPanel spendingsButtonsPanel = new JPanel();
		spendingsButtonsPanel.setLayout(topLayout);
		spendingsButtonsPanel.add(add2);
		spendingsButtonsPanel.add(update2);
		spendingsButtonsPanel.add(delete2);
		spendingsButtonsPanel.add(depositSpendings);
		spendingsButtonsPanel.add(withdrawSpendings);
		spendingsButtonsPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		topPanel.add(spendingsButtonsPanel, BorderLayout.LINE_END);

		// ---------sum panel------------------------
		JPanel sumPanel = new JPanel();
		sumPanel.setLayout(topLayout);
		sumPanel.add(sumLabel);
		sum = new JTextField();
		sum.setColumns(20);
		sumPanel.add(sum);
		sumPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		topPanel.add(sumPanel, BorderLayout.SOUTH);

		add(topPanel, BorderLayout.NORTH);

		// ----------CENTER---------------
		JPanel tablePanel = new JPanel();
		tablePanel.setLayout(topLayout);
		savingsTableModel = new SavingsTableModel();
		savingsTable = new JTable(savingsTableModel);
		JScrollPane savingsScrollPane = new JScrollPane(savingsTable);
		tablePanel.add(savingsScrollPane);

		spendingsTableModel = new SpendingsTableModel();
		spendingsTable = new JTable(spendingsTableModel);
		JScrollPane spendingsScrollPane = new JScrollPane(spendingsTable);
		tablePanel.add(spendingsScrollPane);
		tablePanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		add(tablePanel, BorderLayout.CENTER);

		// ----------BOTTOM-----------
		JPanel bottomPanel = new JPanel();
		FlowLayout bottomLayout = new FlowLayout();
		bottomPanel.setLayout(bottomLayout);
		accountId = new JTextField();
		accountId.setColumns(20);
		accountName = new JTextField();
		accountName.setColumns(20);
		accountSum = new JTextField();
		accountSum.setColumns(20);
		accountSum.setEditable(false);
		accountType = new JTextField();
		accountType.setColumns(20);
		accountType.setEditable(false);
		bottomPanel.add(accountId);
		bottomPanel.add(accountName);
		bottomPanel.add(accountSum);
		bottomPanel.add(accountType);
		bottomPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		add(bottomPanel, BorderLayout.SOUTH);

		// ------SELECTION LISTENERS-----
		savingsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			public void valueChanged(ListSelectionEvent event) {
				int rowIndex = event.getFirstIndex();
				accountId.setText((String) savingsTableModel.getValueAt(rowIndex, 0));
				accountName.setText((String) savingsTableModel.getValueAt(rowIndex, 1));
				accountSum.setText((String) savingsTableModel.getValueAt(rowIndex, 2));
				accountType.setText((String) savingsTableModel.getValueAt(rowIndex, 3));
				oldsavings = new AccountSavings(Integer.parseInt((String) savingsTableModel.getValueAt(rowIndex, 0)),
						(String) savingsTableModel.getValueAt(rowIndex, 1),
						Double.parseDouble((String) savingsTableModel.getValueAt(rowIndex, 2)),
						(String) savingsTableModel.getValueAt(rowIndex, 3), (String) client.getCnp());
			}
		});

		spendingsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			public void valueChanged(ListSelectionEvent event) {
				int rowIndex = event.getFirstIndex();
				accountId.setText((String) spendingsTableModel.getValueAt(rowIndex, 0));
				accountName.setText((String) spendingsTableModel.getValueAt(rowIndex, 1));
				accountSum.setText((String) spendingsTableModel.getValueAt(rowIndex, 2));
				accountType.setText((String) spendingsTableModel.getValueAt(rowIndex, 3));
				oldspendings = new AccountSpendings(
						Integer.parseInt((String) spendingsTableModel.getValueAt(rowIndex, 0)),
						(String) spendingsTableModel.getValueAt(rowIndex, 1),
						Double.parseDouble((String) spendingsTableModel.getValueAt(rowIndex, 2)),
						(String) spendingsTableModel.getValueAt(rowIndex, 3), (String) client.getCnp());

			}
		});

		// --------BUTTON LISTENERS----------
		add1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				savingsManager.addSavingsAccount(client, new AccountSavings(Integer.parseInt(accountId.getText()),
						accountName.getText(), 0, accountType.getText(), clientCnp.getText()));
				loadSavings();
			}
		});

		update1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				savingsManager.updateSavingsAccount(client, oldsavings,
						new AccountSavings(Integer.parseInt(accountId.getText()), accountName.getText(),
								Double.parseDouble(accountSum.getText()), accountType.getText(), clientCnp.getText()));
				loadSavings();
			}
		});

		delete1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				savingsManager.deleteSavingsAccount(client, oldsavings);
				loadSavings();
			}
		});

		depositSavings.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				if (sum.getText().isEmpty()) {
					Component frame = null;
					JOptionPane.showMessageDialog(frame, "Please put a sum!", "Inane error", JOptionPane.ERROR_MESSAGE);
				} else {
					AccountSavings newsavings = oldsavings;
					newsavings.deposit(Double.parseDouble(sum.getText()));
					savingsManager.updateSavingsAccount(client, oldsavings, newsavings);
					loadSavings();
				}
			}
		});

		withdrawSavings.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				AccountSavings newsavings = oldsavings;
				int wsum = 0;
				newsavings.withdraw(wsum);
				savingsManager.updateSavingsAccount(client, oldsavings, newsavings);
				loadSavings();
			}
		});

		add2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				spendingsManager.addSpendingsAccount(client, new AccountSpendings(Integer.parseInt(accountId.getText()),
						accountName.getText(), 0, accountType.getText(), clientCnp.getText()));
				loadSpendings();
			}
		});

		update2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				spendingsManager.updateSpendingsAccount(client, oldspendings,
						new AccountSpendings(Integer.parseInt(accountId.getText()), accountName.getText(),
								Double.parseDouble(accountSum.getText()), accountType.getText(), clientCnp.getText()));
				loadSpendings();
			}
		});

		delete2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				spendingsManager.deleteSpendingsAccount(client, oldspendings);
				loadSpendings();
			}
		});

		depositSpendings.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				if (sum.getText().isEmpty()) {
					Component frame = null;
					JOptionPane.showMessageDialog(frame, "Please put a sum!", "Inane error", JOptionPane.ERROR_MESSAGE);
				} else {
					AccountSpendings newspendings = oldspendings;
					newspendings.deposit(Double.parseDouble(sum.getText()));
					spendingsManager.updateSpendingsAccount(client, oldspendings, newspendings);
					loadSpendings();
				}
			}
		});

		withdrawSpendings.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				if (sum.getText().isEmpty()) {
					Component frame = null;
					JOptionPane.showMessageDialog(frame, "Please put a sum!", "Inane error", JOptionPane.ERROR_MESSAGE);
				} else {
					AccountSpendings newspendings = oldspendings;
					newspendings.withdraw(Double.parseDouble(sum.getText()));
					spendingsManager.updateSpendingsAccount(client, oldspendings, newspendings);
					loadSpendings();
				}
			}
		});

		setSize(2000, 700);
		setLocationRelativeTo(null);
	}

	public void getClient(Person c) {
		clientCnp.setText(c.getCnp());
		clientName.setText(c.getName());
		client = c;
	}

	public void loadSavings() {
		savingsTableModel.load();
	}

	private class SavingsTableModel extends AbstractTableModel {

		/**
		 * 
		 */
		private static final long serialVersionUID = -4962776335013020870L;
		ArrayList<String[]> rows; // will hold String[] objects . . .
		int colCount;
		String[] headers = { "ID", "Name", "Sum", "Type" };

		public SavingsTableModel() {
			rows = new ArrayList<String[]>();
		}

		public String getColumnName(int i) {
			return headers[i];
		}

		public int getColumnCount() {
			return headers.length;
		}

		public int getRowCount() {
			return rows.size();
		}

		public Object getValueAt(int row, int col) {
			return ((String[]) rows.get(row))[col];
		}

		public void load() {
			try {
				rows.clear();
				accounts = savingsManager.getAccounts(client);
				if (!accounts.isEmpty()) {
					for (Account account : accounts) {
						String type = new String("Savings");
						if (account.getType().equals(type)) {
							savings.add((AccountSavings) account);

						}
					}
					for (AccountSavings account : savings) {
						String[] row = new String[4];
						row[0] = Integer.toString(account.getId());
						row[1] = account.getName();
						row[2] = Double.toString(account.getSold());
						row[3] = account.getType();
						rows.add(row);
					}
					fireTableChanged(null); // notify everyone that we have a new table.
				}
			} catch (Exception e) {
				rows = new ArrayList<String[]>();
				e.printStackTrace();
			}
		}

	}

	public void loadSpendings() {
		spendingsTableModel.load();
	}

	private class SpendingsTableModel extends AbstractTableModel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		ArrayList<String[]> rows; // will hold String[] objects . . .
		int colCount;
		String[] headers = { "ID", "Name", "Sum", "Type" };

		public SpendingsTableModel() {
			rows = new ArrayList<String[]>();
		}

		public String getColumnName(int i) {
			return headers[i];
		}

		public int getColumnCount() {
			return headers.length;
		}

		public int getRowCount() {
			return rows.size();
		}

		public Object getValueAt(int row, int col) {
			return ((String[]) rows.get(row))[col];
		}

		public void load() {
			try {
				rows.clear();
				accounts = spendingsManager.getAccounts(client);

				for (Account account : accounts) {
					String type = new String("Spendings");
					if (account.getType().equals(type)) {
						spendings.add((AccountSpendings) account);
					}
				}
				for (AccountSpendings account : spendings) {
					String[] row = new String[4];
					row[0] = Integer.toString(account.getId());
					row[1] = account.getName();
					row[2] = Double.toString(account.getSold());
					row[3] = account.getType();
					rows.add(row);
				}
				fireTableChanged(null); // notify everyone that we have a new table.
			} catch (Exception e) {
				rows = new ArrayList<String[]>();
				e.printStackTrace();
			}
		}

	}
}
