package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.Bank;
import model.Person;

public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8902805237114558598L;
	Person selected = new Person();
	String sel;
	Bank bank = new Bank();
	Set<Person> clientsList;
	private JLabel selectLabel = new JLabel("Select  a client");
	private JButton selectButton = new JButton("Select");
	private JButton manageButton = new JButton("Manage Clients");
	private JComboBox<String> clientCombo = new JComboBox<String>();

	public MainFrame() {

		JPanel firstPanel = new JPanel();
		firstPanel.setLayout(null);
		this.add(firstPanel);

		selectLabel.setBounds(90, 30, 300, 50);
		firstPanel.add(selectLabel);
		selectLabel.setFont(selectLabel.getFont().deriveFont(30.0f));

		manageButton.setBounds(130, 200, 180, 40);
		firstPanel.add(manageButton);
		manageButton.setFont(manageButton.getFont().deriveFont(16.0f));
		manageButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				ManageClientsFrame manageClients = new ManageClientsFrame();
				manageClients.loadClients();
				manageClients.setVisible(true);
			}
		});

		clientCombo.setBounds(30, 110, 180, 40);
		firstPanel.add(clientCombo);
		clientCombo.setFont(clientCombo.getFont().deriveFont(18.0f));
		populateCombo();

		selectButton.setBounds(250, 110, 150, 40);
		firstPanel.add(selectButton);
		selectButton.setFont(selectButton.getFont().deriveFont(18.0f));
		selectButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				AccountsFrame accountsFrame = new AccountsFrame();
				sel = (String) clientCombo.getSelectedItem();

				Component frame = null;
				if (sel != "Please Select...") {
					selected = bank.fromString(sel);
					accountsFrame.getClient(selected);
					accountsFrame.loadSavings();
					accountsFrame.loadSpendings();
					accountsFrame.setVisible(true);
				} else
					JOptionPane.showMessageDialog(frame, "Please select a customer!", "Inane error",
							JOptionPane.ERROR_MESSAGE);

			}
		});

		this.setTitle("Customer Select");
		this.setSize(450, 300);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setContentPane(firstPanel);
		firstPanel.setBackground(new Color(230, 255, 255));
	}

	public void populateCombo() {
		clientsList = bank.getKeys();
		clientCombo.addItem("Please Select...");
		for (Person client : clientsList)
			clientCombo.addItem(client.toString());
	}

}
