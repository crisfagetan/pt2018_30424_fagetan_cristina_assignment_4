package view;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import model.Bank;
import model.Person;

public class ManageClientsFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Bank clientManager;
	private JTable table;
	ClientTableModel tableModel;
	Set<Person> clients;
	private Person oldPers = new Person();

	private JTextField cnp;
	private JTextField nameEdit;
	private JLabel cnpLabel = new JLabel("id");
	private JLabel nameLabel = new JLabel("name");
	private JButton addButton = new JButton("ADD");
	private JButton updateButton = new JButton("UPDATE");
	private JButton deleteButton = new JButton("DELETE");

	public ManageClientsFrame() {
		//
		clientManager = new Bank();

		// top
		JPanel topPanel = new JPanel();
		FlowLayout topLayout = new FlowLayout();
		topPanel.setLayout(topLayout);
		topPanel.add(addButton);
		topPanel.add(updateButton);
		topPanel.add(deleteButton);
		topPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		add(topPanel, BorderLayout.NORTH);

		// center
		tableModel = new ClientTableModel();
		table = new JTable(tableModel);
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane, BorderLayout.CENTER);
		// bottom
		JPanel bottomPanel = new JPanel();
		FlowLayout bottomLayout = new FlowLayout();
		bottomPanel.setLayout(bottomLayout);
		cnp = new JTextField();
		cnp.setColumns(20);
		nameEdit = new JTextField();
		nameEdit.setColumns(20);
		bottomPanel.add(cnp);
		bottomPanel.add(nameEdit);
		bottomPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		add(bottomPanel, BorderLayout.SOUTH);

		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			public void valueChanged(ListSelectionEvent event) {
				int rowIndex = event.getFirstIndex();
				cnp.setText((String) table.getValueAt(rowIndex, 0));
				nameEdit.setText((String) table.getValueAt(rowIndex, 1));
				oldPers = new Person((String) table.getValueAt(rowIndex, 0), (String) table.getValueAt(rowIndex, 1));
			}
		});

		addButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				clientManager.addClient(new Person(cnp.getText(), nameEdit.getText()));
				loadClients();
			}
		});

		updateButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				clientManager.updateClient(oldPers, new Person(cnp.getText(), nameEdit.getText()));
				loadClients();
			}
		});

		deleteButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				clientManager.deleteClient(oldPers);
				loadClients();
			}
		});

		this.setTitle("Client Manager");
		this.setSize(2000, 700);
		this.setLocationRelativeTo(null);

	}

	public void loadClients() {
		tableModel.load();
	}

	private class ClientTableModel extends AbstractTableModel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		ArrayList<String[]> rows; // will hold String[] objects . . .
		int colCount;
		String[] headers = { "CNP", "Name" };

		public ClientTableModel() {
			rows = new ArrayList<String[]>();
		}

		public String getColumnName(int i) {
			return headers[i];
		}

		public int getColumnCount() {
			return headers.length;
		}

		public int getRowCount() {
			return rows.size();
		}

		public Object getValueAt(int row, int col) {
			return ((String[]) rows.get(row))[col];
		}

		public void load() {
			try {
				rows.clear();
				clients = clientManager.getKeys();
				for (Person client : clients) {
					String[] row = new String[2];
					row[0] = client.getCnp();
					row[1] = client.getName();
					rows.add(row);
				}
				fireTableChanged(null); // notify everyone that we have a new table.
			} catch (Exception e) {
				rows = new ArrayList<String[]>();
				e.printStackTrace();
			}
		}
	}
}
