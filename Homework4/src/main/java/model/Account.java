package model;

import java.awt.Component;
import java.io.Serializable;

import javax.swing.JOptionPane;

@SuppressWarnings("deprecation")
public class Account implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6881489033354930576L;

	private int id;
	private String name;
	private double sold;
	private String type;
	private String clientCnp;

	public Account(int id, String name, double sold, String type, String clientCnp) {
		super();
		this.id = id;
		this.name = name;
		this.sold = sold;
		this.type = type;
		this.clientCnp = clientCnp;
	}

	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void deposit(double sum) {
		sold = sum + sold;
	}

	public void withdraw(double sum) {
		if (sum <= sold)
			sold = sold - sum;
		else {
			Component frame = null;
			JOptionPane.showMessageDialog(frame, "You don't have that amount in your account! Select a smaller sum!",
					"Inane error", JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getSold() {
		return sold;
	}

	public void setSold(double sold) {
		this.sold = sold;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClientCnp() {
		return clientCnp;
	}

	public void setClientCnp(String clientCnp) {
		this.clientCnp = clientCnp;
	}

	@Override
	public String toString() {
		return "ID: " + id + "; Name: " + name + "; Sold: " + sold + "; Type: " + type + "; Client: " + clientCnp;
	}

}
