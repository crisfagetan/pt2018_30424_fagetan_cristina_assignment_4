package model;

import java.io.Serializable;

public class Person implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = -6403404616117600547L;
	private String cnp;
	private String name;

	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Person(String cnp, String name) {
		super();
		this.cnp = cnp;
		this.name = name;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cnp == null) ? 0 : cnp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (cnp == null) {
			if (other.cnp != null)
				return false;
		} else if (!cnp.equals(other.cnp))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Client: " + name + "; CNP: " + cnp;
	}

}
