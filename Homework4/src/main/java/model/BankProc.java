package model;

public interface BankProc {
	
	/**
	 * adds a new client to the bank
	 * @param person is the client the user want to add to the bank
	 * @pre person is not null
	 * @post bank contains person
	 */
	public void addClient(Person person);
	
	/**
	 * updates a client in the bank
	 * @param oldPers is the client the user wants to change
	 * @param newPers is the new value of the client
	 * @pre bank contains oldPers
	 * @pre newPers is not null
	 * @post bank contains newPers
	 * @post bank does not contain oldPers
	 */
	public void updateClient(Person oldPers, Person newPers);
	
	/**
	 * deletes a client from bank
	 * @param person is the person to be deleted
	 * @pre bank contains person
	 * @pre person is not null
	 * @post bank does not contain person
	 */
	public void deleteClient(Person person);
	
	/**
	 * adds a spendings account to the person
	 * @param person is the client the account will be added to
	 * @param spendings is the account to be added
	 * @pre person exists in bank
	 * @post spendings is an account associated to the client person
	 */
	public void addSpendingsAccount(Person person, AccountSpendings spendings);
	
	/**
	 * updates a spendings account from a client
	 * @param person is the client that has the account
	 * @param oldspendings is the account to be updated
	 * @param newspendings is the new value of the account
	 * @pre person is in the bank
	 * @pre oldspendings is an account of the client person
	 * @post newspendings is an account of the client person
	 * @post oldspendings no longer exists
	 * @post person is in the bank
	 */
	public void updateSpendingsAccount(Person person, AccountSpendings oldspendings, AccountSpendings newspendings);
	
	/**
	 * deletes a spendings account from a client
	 * @param person is the client
	 * @param spendings the account to be deleted
	 * @pre person is in the bank
	 * @pre spendings is an account the client has
	 * @post spendings no longer exist
	 * @post person is in the bank
	 */
	public void deleteSpendingsAccount(Person person, AccountSpendings spendings);
	
	/**
	 * adds a savings account
	 * @param person is the client that has the account
	 * @param savings  is the account to be added
	 * @pre person exists in bank
	 * @post savings is an account associated to the client person
	 */
	public void addSavingsAccount(Person person, AccountSavings savings);
	
	/**
	 * updates a savings account from a client
	 * @param person is the client that has the account
	 * @param oldspendings is the account to be updated
	 * @param newspendings is the new value of the account
	 * @pre person is in the bank
	 * @pre oldsavings is an account of the client person
	 * @post newsavings is an account of the client person
	 * @post oldsavings no longer exists
	 * @post person is in the bank
	 */
	public void updateSavingsAccount(Person person, AccountSavings oldsavings, AccountSavings newsavings);
	
	/**
	 * deletes a savings account from a client
	 * @param person is the client
	 * @param spendings the account to be deleted
	 * @pre person is in the bank
	 * @pre savings is an account the client has
	 * @post savings no longer exist
	 * @post person is in the bank
	 */
	public void deleteSavingsAccount(Person person, AccountSavings savings);
}
