package model;

import java.awt.Component;

import javax.swing.JOptionPane;

public class AccountSavings extends Account {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1300440469110530045L;
	private static double interest = 0.21;
	// tells if you can deposit money here, since you have a single use
	private boolean deposit = true;
	private boolean withdraw = true;

	public AccountSavings() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountSavings(int id, String name, double sold, String type, String clientCnp) {
		super(id, name, sold, "Savings", clientCnp);
		deposit = (sold == 0) ? true : false;
	}

	@Override
	public void deposit(double sum) {
		if (deposit) {
			setDeposit(false);
			super.deposit(sum);
			interest();

		} else {
			Component frame = null;
			JOptionPane.showMessageDialog(frame, "Already deposited once! You can only withdraw now!", "Inane error",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	public void withdraw(double sum) {
		if (withdraw) {
			setWithdraw(false);
			super.withdraw(getSold());
		} else {
			Component frame = null;
			JOptionPane.showMessageDialog(frame, "Already withdrawed once! No more actions available for this account!",
					"Inane error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void interest() {
		setSold(getSold() + getSold() * interest);
	}

	public boolean isDeposit() {
		return deposit;
	}

	public void setDeposit(boolean deposit) {
		this.deposit = deposit;
	}

	public boolean isWithdraw() {
		return withdraw;
	}

	public void setWithdraw(boolean withdraw) {
		this.withdraw = withdraw;
	}

}
