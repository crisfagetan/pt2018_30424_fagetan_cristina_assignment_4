package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;

public class SerializationFile {

	public void serialize(HashMap<Person, HashSet<Account>> obj) {
		try {
			FileOutputStream fileOut = new FileOutputStream("src/main/java/model/Bank.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(obj);
			out.close();
			fileOut.close();
		} catch (IOException e) {
			System.out.println("Serialization failed");
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	public HashMap<Person, HashSet<Account>> deserialize() {
		HashMap<Person, HashSet<Account>> map = null;
		try {
			FileInputStream fis = new FileInputStream("src/main/java/model/Bank.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			map = (HashMap<Person, HashSet<Account>>) ois.readObject();
			ois.close();
			fis.close();
		} catch (Exception e) {
			System.out.println("Deserialization failed");
			System.out.println(e.getMessage());
		}
		return map;

	}

}
