package model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Bank implements BankProc {

	public SerializationFile ser = new SerializationFile();
	public HashMap<Person, HashSet<Account>> bank;
	public HashSet<Person> clients = new HashSet<>();
	public HashSet<Account> accounts = new HashSet<>();

	public Bank() {
		super();
		this.bank = new HashMap<>();
		try {
			bank = ser.deserialize();
		} catch (Exception e) {
			e.printStackTrace();
		}
		// TODO Auto-generated constructor stub
	}

	public Person fromString(String str) {
		Person res = new Person();
		str = str.substring(8);
		String array[] = str.split("; CNP: ");
		res.setCnp(array[1]);
		res.setName(array[0]);
		return res;
	}

	public Set<Person> getKeys() {
		return bank.keySet();
	}

	public HashSet<Account> getAccounts(Person key) {
		return bank.get(key);
	}

	public void addClient(Person person) {
		assert person.equals(null) == false;
		HashSet<Account> a = new HashSet<>();
		bank.putIfAbsent(person, a);
		assert bank.containsKey(person);
		ser.serialize(bank);
	}

	public void updateClient(Person oldPers, Person newPers) {
		assert newPers.equals(null) == false;
		assert bank.containsKey(oldPers);
		accounts = bank.remove(oldPers);
		bank.put(newPers, accounts);
		assert bank.containsKey(newPers);
		assert bank.containsKey(oldPers) == false;
		ser.serialize(bank);
	}

	public void deleteClient(Person person) {
		assert bank.containsKey(person);
		accounts = bank.get(person);
		bank.remove(person, accounts);
		assert bank.containsKey(person) == false;
		ser.serialize(bank);
	}

	public void addSpendingsAccount(Person person, AccountSpendings spendings) {
		assert bank.containsKey(person);
		accounts = bank.get(person);
		accounts.add(spendings);
		bank.put(person, accounts);
		assert bank.get(person).contains(spendings);
		ser.serialize(bank);
	}

	public void updateSpendingsAccount(Person person, AccountSpendings oldspendings, AccountSpendings newspendings) {
		assert bank.containsKey(person);
		assert bank.get(person).contains(oldspendings);
		accounts = bank.get(person);
		accounts.remove(oldspendings);
		accounts.add(newspendings);
		bank.put(person, accounts);
		assert bank.containsKey(person);
		assert bank.get(person).contains(newspendings);
		assert bank.get(person).contains(oldspendings) == false;
		ser.serialize(bank);
	}

	public void deleteSpendingsAccount(Person person, AccountSpendings spendings) {
		assert bank.containsKey(person);
		assert bank.get(person).contains(spendings);
		accounts = bank.get(person);
		accounts.remove(spendings);
		bank.put(person, accounts);
		assert bank.containsKey(person);
		assert bank.get(person).contains(spendings) == false;
		ser.serialize(bank);
	}

	public void addSavingsAccount(Person person, AccountSavings savings) {
		assert bank.containsKey(person);
		accounts = bank.get(person);
		accounts.add(savings);
		bank.put(person, accounts);
		assert bank.get(person).contains(savings);
		ser.serialize(bank);
	}

	public void updateSavingsAccount(Person person, AccountSavings oldsavings, AccountSavings newsavings) {
		assert bank.containsKey(person);
		assert bank.get(person).contains(oldsavings);
		accounts = bank.get(person);
		accounts.remove(oldsavings);
		accounts.add(newsavings);
		bank.put(person, accounts);
		assert bank.containsKey(person);
		assert bank.get(person).contains(newsavings);
		assert bank.get(person).contains(oldsavings) == false;
		ser.serialize(bank);
	}

	public void deleteSavingsAccount(Person person, AccountSavings savings) {
		assert bank.containsKey(person);
		assert bank.get(person).contains(savings);
		accounts = bank.get(person);
		accounts.remove(savings);
		bank.put(person, accounts);
		assert bank.containsKey(person);
		assert bank.get(person).contains(savings) == false;
		ser.serialize(bank);
	}

}
